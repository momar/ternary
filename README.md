# ternary.go
A tiny ternary operator for the Go language.

## Usage

```
import (
    "codeberg.org/momar/ternary"
)
```

**`If(cond, yes, no).(type)`** returns **yes** if **cond** is *true*, otherwise it returns **no**.  
Example: `ternary.If(err != nil, "error", "no error").(string)`

**`Default(yes, no).(type)`** returns **yes** if **yes** is a *non-zero value*, otherwise it returns **no**.  
Example: `ternary.Default(os.Getenv("URL"), "https://example.org").(string)`

**`PanicIf(cond, v)`** panics with **v** if **cond** is *true*, otherwise it does nothing.  
Example: `ternary.PanicIf(err != nil, err)`  
**WARNING:** You should use panics as rarely as possible!

**`Unwrap(value, err)`** panics with **err** if it is not *nil*, otherwise it returns **value**.  
Example: `ternary.Unwrap(http.Get("https://checkip.amazonaws.com")).(*http.Response)`  
**WARNING:** You should use panics as rarely as possible!

## Example
```go
url := ternary.Default(os.Getenv("URL"), "https://checkip.amazonaws.com/").(string)
res, err := http.Get(url)
ternary.PanicIf(err != nil, err)
ip, _ := ioutil.ReadAll(res.Body)
fmt.Printf("IP: %s\n", ternary.If(res.StatusCode == 200, ip, "Unknown"))
```

## The code golf version
```go
fmt.Printf("IP: %s\n", ternary.Unwrap(ioutil.ReadAll(ternary.Unwrap(http.Get(ternary.Default(os.Getenv("URL"), "https://checkip.amazonaws.com/").(string))).(*http.Response).Body)).([]byte))
```
