package ternary

import (
	"reflect"
)

// If returns yes if cond is true, otherwise it returns no. Example: ternary.If(err != nil, "error", "no error").(string)
func If(cond bool, yes interface{}, no interface{}) interface{} {
	if cond {
		return yes
	}
	return no
}

// Default returns yes if yes is a non-zero value, otherwise it returns no. Example: ternary.Default(os.Getenv("URL"), "https://example.org").(string)
func Default(yes interface{}, no interface{}) interface{} {
	if yes == nil || reflect.DeepEqual(yes, reflect.Zero(reflect.TypeOf(yes)).Interface()) {
		return no
	}
	return yes
}

// PanicIf panics with v if cond is true, otherwise it does nothing. Example: ternary.PanicIf(err != nil, err)
func PanicIf(cond bool, v interface{}) {
	if cond {
		panic(v)
	}
}

// Unwrap panics with err if it is not nil, otherwise it returns value. Example: ternary.Unwrap(http.Get("https://checkip.amazonaws.com")).(*http.Response)
func Unwrap(value interface{}, err error) interface{} {
	PanicIf(err != nil, err)
	return value
}
